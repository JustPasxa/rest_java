package com.java.python_is_forever.java_rest;

import com.java.python_is_forever.java_rest.unique_key_model.KeyUnique;
import com.java.python_is_forever.java_rest.unique_key_model.KeyUniqueRepository;
import javafx.application.Application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import sun.net.www.http.KeepAliveCache;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;


@SpringBootApplication
public class JavaRestApplication {

	public static void main(String[] args) {

	    ConfigurableApplicationContext context = SpringApplication.run(JavaRestApplication.class, args);
        KeyUniqueRepository repository = context.getBean(KeyUniqueRepository.class);

		Thread thread_checker_bd = new Thread(new Runnable()
		{
		    KeyUniqueRepository _repository = repository;
			public void run()
			{
			    while (true)
                {
                    try {
                        Date yesterday = new Date(System.currentTimeMillis()-24*60*60*1000);
                        List<KeyUnique> keys = _repository.findByDeliveryDateLessThanEqual(yesterday);
                        _repository.deleteAll(keys);
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
			}
		});
        thread_checker_bd.start();
	}
}
