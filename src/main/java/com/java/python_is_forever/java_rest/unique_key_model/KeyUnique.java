package com.java.python_is_forever.java_rest.unique_key_model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.Date;


@Entity
public class KeyUnique {
    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long id;

    @Column(name = "Key")
    public String key;
    @Column(name = "Status")
    public String status;
    @Column(name = "DeliveryDate")
    public Date deliveryDate;

    public String ge_status(String key) {
        return status;
    }

}
