package com.java.python_is_forever.java_rest.unique_key_model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;


@Repository
public interface KeyUniqueRepository extends CrudRepository<KeyUnique, Long> {
    KeyUnique findByKey(String key);
    List<KeyUnique> findByStatus(String key);
    List<KeyUnique> findByDeliveryDateLessThanEqual(Date delivery_date);
}