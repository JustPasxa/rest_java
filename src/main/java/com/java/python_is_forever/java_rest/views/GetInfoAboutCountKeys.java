package com.java.python_is_forever.java_rest.views;

import com.java.python_is_forever.java_rest.unique_key_model.KeyUnique;
import com.java.python_is_forever.java_rest.unique_key_model.KeyUniqueRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class GetInfoAboutCountKeys {
    final KeyUniqueRepository key_unique_repository;

    public GetInfoAboutCountKeys(KeyUniqueRepository key_unique_repository) {
        this.key_unique_repository = key_unique_repository;
    }

    @RequestMapping(value = "/get_info_about_count_keys",
            method = RequestMethod.GET,
            produces = "application/json")
    public Map get_info_about_count_keys()
    {
        List<KeyUnique> keys_delivery = this.key_unique_repository.findByStatus("выдан");
        List<KeyUnique> keys_not_valid = this.key_unique_repository.findByStatus("погашен");
        HashMap<String, Integer> map = new HashMap<>();
        map.put("выдано", keys_delivery.size());
        map.put("погашено", keys_not_valid.size());
        return map;
    }
}
