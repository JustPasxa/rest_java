package com.java.python_is_forever.java_rest.views;

import com.java.python_is_forever.java_rest.unique_key_model.KeyUnique;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.java.python_is_forever.java_rest.unique_key_model.KeyUniqueRepository;

import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Calendar;
import java.util.Date;


@RestController
public class GetKeyController {
    final KeyUniqueRepository key_unique_repository;
    final RandomStringUtils random_string_utils;

    public GetKeyController(KeyUniqueRepository key_unique_repository) {
        this.key_unique_repository = key_unique_repository;
        this.random_string_utils = new RandomStringUtils();
    }

    @RequestMapping(value = "/get_unique_key",
            method = RequestMethod.GET,
            produces = "application/json")
    public KeyUnique get_unique_key(){
        String unique_key = random_string_utils.randomAlphanumeric(4);
        Iterable<KeyUnique> keys = key_unique_repository.findAll();
        if (((Collection<?>)keys).size() != 0)
        {
            KeyUnique key_is_exist = key_unique_repository.findByKey(unique_key);
            while (key_is_exist != null)
            {
                unique_key = random_string_utils.randomAlphanumeric(4);
                key_is_exist = key_unique_repository.findByKey(unique_key);
            }
        }
        Date today = new Date(System.currentTimeMillis());

        KeyUnique key_unique = new KeyUnique();
        key_unique.key = unique_key;
        key_unique.status = "выдан";
        key_unique.deliveryDate = today;
        key_unique_repository.save(key_unique);

        return key_unique;
    }
}
