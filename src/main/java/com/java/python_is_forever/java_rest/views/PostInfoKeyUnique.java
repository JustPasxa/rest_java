package com.java.python_is_forever.java_rest.views;


import com.java.python_is_forever.java_rest.unique_key_model.KeyUnique;
import com.java.python_is_forever.java_rest.unique_key_model.KeyUniqueRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class PostInfoKeyUnique {
    final KeyUniqueRepository key_unique_repository;
    final KeyUnique key_unique;

    public PostInfoKeyUnique(KeyUniqueRepository key_unique_repository) {
        this.key_unique_repository = key_unique_repository;
        this.key_unique = new KeyUnique();
    }

    @RequestMapping(value = "/api/{key_unique}", method = RequestMethod.POST)
    public Map get_status_about_key(@PathVariable String key_unique)
    {
        KeyUnique key = key_unique_repository.findByKey(key_unique);
        //хранение данных в виде пар ключ/значение
        HashMap<String, String> map = new HashMap<>();
        map.put(key_unique, key.status);
        return map;
    }
}
