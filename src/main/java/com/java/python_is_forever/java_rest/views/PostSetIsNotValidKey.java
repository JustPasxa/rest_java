package com.java.python_is_forever.java_rest.views;

import com.java.python_is_forever.java_rest.unique_key_model.KeyUnique;
import com.java.python_is_forever.java_rest.unique_key_model.KeyUniqueRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class PostSetIsNotValidKey {
    final KeyUniqueRepository key_unique_repository;

    public PostSetIsNotValidKey(KeyUniqueRepository key_unique_repository) {
        this.key_unique_repository = key_unique_repository;
    }

    @RequestMapping(value = "/api/set_is_not_valid/{key_unique}", method = RequestMethod.POST)
    public Map post_set_is_not_valid_key(@PathVariable String key_unique)
    {
        HashMap<String, String> map = new HashMap<>();
        KeyUnique key = key_unique_repository.findByKey(key_unique);
        Boolean v = key.status.equals("погашен");
        if (key.status.equals("погашен"))
        {
            map.put(key_unique, "уже погашен");
            return map;
        }
        key.status = "погашен";
        this.key_unique_repository.save(key);
        map.put(key_unique, "погашен");
        return map;
    }

}
